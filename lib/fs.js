const fs            = require('fs');
const rmdir         = require('rmdir');
const fx            = require('mkdir-recursive');
export function cleanup(dir){
  console.log("cleaning up old sdk build");
  return new Promise((resolve, reject) => {
    if(!is_directory(dir)) resolve();
    rmdir(dir, (err, result) => {
      if(err) reject(err);
      resolve(result);
    });
  })
  .then(()=>fx.mkdirSync(dir+'/lib'));
}

export function writeFile(target, content) {
  console.log("writing to", target);
  const wr = fs.createWriteStream(target);
  return new Promise((resolve, reject) => {
    wr.on("open", function(){
      wr.write(content);
      wr.end();
    })

    wr.on("error", function(err) {
      reject(err);
    });
    wr.on("close", function(ex) {
      resolve(ex);
    });
  })
}

export function copyFile(source, target) {
  console.log("copying", source, "to", target);
  const rd = fs.createReadStream(source);
  const wr = fs.createWriteStream(target);
  return new Promise((resolve, reject) => {
    rd.on("error", function(err) {
      reject(err);
    });
    wr.on("error", function(err) {
      reject(err);
    });
    wr.on("close", function(ex) {
      resolve();
    });
    rd.pipe(wr);
 })
}

export function is_directory(loc){
  try {
    const stat = fs.statSync(loc);
    return stat.isDirectory()
  }catch(e){
    if(e.code = 'ENOENT'){
      return false
    }else{
      throw(e);
    }
  } 
}

