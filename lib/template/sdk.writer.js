import toCamelCase from './lib/to_camel_case';

export default function sdk_template(name){
  const api_name   = toCamelCase(name);
  const api_routes = `${api_name}API`;
  const sdk_name   = `${api_name}SDK`;
  const filename   = `router.${name}.js`

return `
var ${api_name}Definition = require('./lib/routes.${name}');

var ${api_name}    = new ${api_name}Definition();

var SDKarate    = require('sdkarate')

var axios       = require('axios');

function routify(route_path, args){
  var arg;
  for(arg in args){
    route_path = route_path.replace(":"+arg, args[arg]);
  }
  return route_path;
}

function make_url(url){
  return url.scheme+url.host+':'+url.port+url.path
}
                     
var ${api_name}SDK = SDKarate(${api_name}, {}, (function(${name}){
   return function(api, action, SDK){
      SDK[action] = function(options){
        return ${name} 
              .__definition
              .then(function(def){
                var route = ${name}.__methods[action]
                var url = make_url(def._options);
                return axios.create({
                      baseURL: url,
                      headers: def._options.headers,
                      validateStatus: function (status) {
                                        return status >= 200 && status < 300; // Reject only if the status code is greater than or equal to 500
                                      }
                  }).request({
                    method: route.method,
                    url: routify(route.path, options.route),
                    params: options.query,
                    data: options.body,
                    headers: options.headers||{}
                  })
              })
              .then(function(result){ return result.data } )
              .catch(function(response){
                return Promise.reject({data: response.data,
                                       request: options,
                                       status: response.status,
                                       statusText: response.statusText,
                                       headers: response.headers,
                                       config: response.config } )
              })
    }
    return SDK;
  }
})(${api_name}Definition));

export default ${sdk_name}
`
}
