import toCamelCase from './lib/to_camel_case';

export default function router_template(name){

  const router_name = toCamelCase(name);
  const sdk_name    = `${router_name}SDK`;
  const filename    = `router.${name}.js`
 
  return filename, `
import ${router_name} from './lib/routes.${name}';
import ${sdk_name} from './runner.${name}';

import RouteManager from 'one-track';
import Routify      from './lib/routify_sdk';
import SDKrunner    from './lib/sdk_runner';

const ${name}Router = Routify(${sdk_name},
                               new RouteManager(),
                               ${router_name}.__methods,
                               SDKrunner);

export default ${name}Router;
`
}
