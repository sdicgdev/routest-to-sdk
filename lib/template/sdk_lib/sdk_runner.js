export default function runner(sdk_function, params, headers, body, qstring, ctx){ 
  delete headers.host;
  return sdk_function({
    body: body,
    headers: headers,
    route: params,
    query: qstring
  })
  .result
  .then(r => {
    ctx.response.type = r.headers['content-type'];
    ctx.response.set('transfer-encoding', r.headers['transfer-encoding']);
    if(r.headers['warning']) ctx.response.set('warning', r.headers['warning']);
    ctx.response.status = r.code;
    return r.response
  })
}

function objectify(string){
  return found_pairs({}, ...string.split("&"))
}

function found_pairs(result, pair, ...pairs){
  if(!pair) return result;
  const split  = pair.split("=");
  let new_pair =  {};
  new_pair[split[0]] = decodeURIComponent(split[1]);
  return found_pairs(Object.assign({}, result, new_pair), ...pairs);
}
