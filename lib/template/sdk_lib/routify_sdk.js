
export default function RoutifySDK(sdk, router, definition, runner,
                               methods=Object.keys(definition) ){

  const entry      = methods.slice(0, 1);
  const remaining  = methods.slice(1);
  
  if(entry.length){
    const route = definition[entry[0]];
    return RoutifySDK( sdk,
                        router[route.method](
                          route.path,
                          function(){
                            return runner.call(this, sdk[entry[0]], ...arguments); 
                          }
                        ),
                        definition,
                        runner,
                        remaining
                      );
  }else{
    return router
  }
}
