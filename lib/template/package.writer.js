export default function package_file_template(name, version){
  return `
  {
    "name": "${name}",
    "version": "${version}",
    "dependencies": {
      "axios": "^0.11.0",
      "routest": "^0.3",
      "sdkarate": "0.0.3"
    }
  }
  `
}
