import toCamelCase from './lib/to_camel_case';

export default function sdk_template(name){
  const api_name   = toCamelCase(name);
  const api_routes = `${api_name}API`;
  const sdk_name   = `${api_name}SDK`;
  const filename   = `router.${name}.js`

return `
var SDKarate = require('sdkarate');
var ${api_routes} = require('./lib/routes.${name}');

var ${api_name} = new ${api_routes}();

var BaseSDK = SDKarate(${api_name});

var ${sdk_name} = Object.keys(BaseSDK).reduce(function(prev, cur){
  prev[cur] = function(options){
  if(options.headers) delete options.headers['content-length'];
  return BaseSDK[cur](options);
  }
  return prev;
}, {})

export default ${sdk_name}
`
}
