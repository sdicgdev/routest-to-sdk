import {writeFile, copyFile} from './fs'


export default function build_sdk(opts, ...remaining){
  const template_dir   = __dirname+'/template';
  const sdk_writer     = require(template_dir+'/sdk.writer.js').default;
  const runner_writer  = require(template_dir+'/runner.writer.js').default;
  const router_writer  = require(template_dir+'/router.writer.js').default;
  if(opts){
    return copyFile(template_dir+'/sdk_lib/routify_sdk.js', opts.SDK_LIB+'/routify_sdk.js')
      .then(_ => copyFile(template_dir+'/sdk_lib/sdk_runner.js', opts.SDK_LIB+'/sdk_runner.js'))
      .then(_ => copyFile(opts.routes_loc, opts.SDK_LIB+'/routes.'+opts.package_name+'.js'))
      .then(_ => writeFile(opts.RUNNER_FILE, runner_writer(opts.package_name)))
      .then(_ => writeFile(opts.SDK_FILE, sdk_writer(opts.package_name)))
      .then(_ => writeFile(opts.ROUTER_FILE, router_writer(opts.package_name)))
      .then(_ => build_sdk(...remaining))
      .catch((err) => console.log(err.stack))
      ;
  }
}

export function define_package(loc, info){
  const template_dir   = __dirname+'/template';
  const package_writer = require(template_dir+'/package.writer.js').default;
  return writeFile(loc, package_writer(info.name, info.version))
}
