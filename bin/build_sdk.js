#!/usr/bin/env node
import { cleanup, is_directory } from '../lib/fs';
import build_sdk, { define_package } from '../lib/build_sdk';

const BUILD_DIR     = process.cwd()+'/build/sdk';
const SDK_LIB       = BUILD_DIR+'/lib';
const package_info  = require(process.cwd()+'/package.json');
const sdk_info      = package_info.sdk_info;
if(Array.isArray(sdk_info)){
  const info_array = sdk_info;
}else{
  const info_array = [sdk_info];
}

const opts_ray = sdk_info.map(info => {
  let package_name  = info.name; 
  let routes_loc    = process.cwd()+'/'+info.definition;
  let SDK_FILE      = BUILD_DIR+'/sdk.'+package_name+'.js';
  let ROUTER_FILE   = BUILD_DIR+'/router.'+package_name+'.js';
  let RUNNER_FILE   = BUILD_DIR+'/runner.'+package_name+'.js';
  return {
    package_name,
    routes_loc,
    BUILD_DIR,
    SDK_LIB,
    SDK_FILE,
    ROUTER_FILE,
    RUNNER_FILE
  }
})

cleanup(BUILD_DIR)
  .then(_ => build_sdk(...opts_ray))
  .then(_ => define_package(BUILD_DIR+'/package.json', package_info))
  .catch(e => {
    console.log("ERROR:", e)
    process.exit(1);
  })
