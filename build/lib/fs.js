'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.cleanup = cleanup;
exports.writeFile = writeFile;
exports.copyFile = copyFile;
exports.is_directory = is_directory;
var fs = require('fs');
var rmdir = require('rmdir');
var fx = require('mkdir-recursive');
function cleanup(dir) {
  console.log("cleaning up old sdk build");
  return new Promise(function (resolve, reject) {
    if (!is_directory(dir)) resolve();
    rmdir(dir, function (err, result) {
      if (err) reject(err);
      resolve(result);
    });
  }).then(function () {
    return fx.mkdirSync(dir + '/lib');
  });
}

function writeFile(target, content) {
  console.log("writing to", target);
  var wr = fs.createWriteStream(target);
  return new Promise(function (resolve, reject) {
    wr.on("open", function () {
      wr.write(content);
      wr.end();
    });

    wr.on("error", function (err) {
      reject(err);
    });
    wr.on("close", function (ex) {
      resolve(ex);
    });
  });
}

function copyFile(source, target) {
  console.log("copying", source, "to", target);
  var rd = fs.createReadStream(source);
  var wr = fs.createWriteStream(target);
  return new Promise(function (resolve, reject) {
    rd.on("error", function (err) {
      reject(err);
    });
    wr.on("error", function (err) {
      reject(err);
    });
    wr.on("close", function (ex) {
      resolve();
    });
    rd.pipe(wr);
  });
}

function is_directory(loc) {
  try {
    var stat = fs.statSync(loc);
    return stat.isDirectory();
  } catch (e) {
    if (e.code = 'ENOENT') {
      return false;
    } else {
      throw e;
    }
  }
}