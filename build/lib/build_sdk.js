'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = build_sdk;
exports.define_package = define_package;

var _fs = require('./fs');

function build_sdk(opts) {
  for (var _len = arguments.length, remaining = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    remaining[_key - 1] = arguments[_key];
  }

  var template_dir = __dirname + '/template';
  var sdk_writer = require(template_dir + '/sdk.writer.js').default;
  var runner_writer = require(template_dir + '/runner.writer.js').default;
  var router_writer = require(template_dir + '/router.writer.js').default;
  if (opts) {
    return (0, _fs.copyFile)(template_dir + '/sdk_lib/routify_sdk.js', opts.SDK_LIB + '/routify_sdk.js').then(function (_) {
      return (0, _fs.copyFile)(template_dir + '/sdk_lib/sdk_runner.js', opts.SDK_LIB + '/sdk_runner.js');
    }).then(function (_) {
      return (0, _fs.copyFile)(opts.routes_loc, opts.SDK_LIB + '/routes.' + opts.package_name + '.js');
    }).then(function (_) {
      return (0, _fs.writeFile)(opts.RUNNER_FILE, runner_writer(opts.package_name));
    }).then(function (_) {
      return (0, _fs.writeFile)(opts.SDK_FILE, sdk_writer(opts.package_name));
    }).then(function (_) {
      return (0, _fs.writeFile)(opts.ROUTER_FILE, router_writer(opts.package_name));
    }).then(function (_) {
      return build_sdk.apply(undefined, remaining);
    }).catch(function (err) {
      return console.log(err.stack);
    });
  }
}

function define_package(loc, info) {
  var template_dir = __dirname + '/template';
  var package_writer = require(template_dir + '/package.writer.js').default;
  return (0, _fs.writeFile)(loc, package_writer(info.name, info.version));
}