'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = router_template;

var _to_camel_case = require('./lib/to_camel_case');

var _to_camel_case2 = _interopRequireDefault(_to_camel_case);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function router_template(name) {

  var router_name = (0, _to_camel_case2.default)(name);
  var sdk_name = router_name + 'SDK';
  var filename = 'router.' + name + '.js';

  return filename, '\nimport ' + router_name + ' from \'./lib/routes.' + name + '\';\nimport ' + sdk_name + ' from \'./runner.' + name + '\';\n\nimport RouteManager from \'one-track\';\nimport Routify      from \'./lib/routify_sdk\';\nimport SDKrunner    from \'./lib/sdk_runner\';\n\nconst ' + name + 'Router = Routify(' + sdk_name + ',\n                               new RouteManager(),\n                               ' + router_name + '.__methods,\n                               SDKrunner);\n\nexport default ' + name + 'Router;\n';
}