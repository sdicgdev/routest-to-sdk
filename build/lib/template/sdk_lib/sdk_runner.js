'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = runner;

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function runner(sdk_function, params, headers, body, qstring, ctx) {
  delete headers.host;
  return sdk_function({
    body: body,
    headers: headers,
    route: params,
    query: qstring
  }).result.then(function (r) {
    ctx.response.type = r.headers['content-type'];
    ctx.response.set('transfer-encoding', r.headers['transfer-encoding']);
    if (r.headers['warning']) ctx.response.set('warning', r.headers['warning']);
    ctx.response.status = r.code;
    return r.response;
  });
}

function objectify(string) {
  return found_pairs.apply(undefined, [{}].concat(_toConsumableArray(string.split("&"))));
}

function found_pairs(result, pair) {
  if (!pair) return result;
  var split = pair.split("=");
  var new_pair = {};
  new_pair[split[0]] = decodeURIComponent(split[1]);

  for (var _len = arguments.length, pairs = Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
    pairs[_key - 2] = arguments[_key];
  }

  return found_pairs.apply(undefined, [Object.assign({}, result, new_pair)].concat(pairs));
}