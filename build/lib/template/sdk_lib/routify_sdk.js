"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = RoutifySDK;
function RoutifySDK(sdk, router, definition, runner) {
  var methods = arguments.length <= 4 || arguments[4] === undefined ? Object.keys(definition) : arguments[4];


  var entry = methods.slice(0, 1);
  var remaining = methods.slice(1);

  if (entry.length) {
    var route = definition[entry[0]];
    return RoutifySDK(sdk, router[route.method](route.path, function () {
      return runner.call.apply(runner, [this, sdk[entry[0]]].concat(Array.prototype.slice.call(arguments)));
    }), definition, runner, remaining);
  } else {
    return router;
  }
}