'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = sdk_template;

var _to_camel_case = require('./lib/to_camel_case');

var _to_camel_case2 = _interopRequireDefault(_to_camel_case);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function sdk_template(name) {
  var api_name = (0, _to_camel_case2.default)(name);
  var api_routes = api_name + 'API';
  var sdk_name = api_name + 'SDK';
  var filename = 'router.' + name + '.js';

  return '\nvar SDKarate = require(\'sdkarate\');\nvar ' + api_routes + ' = require(\'./lib/routes.' + name + '\');\n\nvar ' + api_name + ' = new ' + api_routes + '();\n\nvar BaseSDK = SDKarate(' + api_name + ');\n\nvar ' + sdk_name + ' = Object.keys(BaseSDK).reduce(function(prev, cur){\n  prev[cur] = function(options){\n  if(options.headers) delete options.headers[\'content-length\'];\n  return BaseSDK[cur](options);\n  }\n  return prev;\n}, {})\n\nexport default ' + sdk_name + '\n';
}