"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = package_file_template;
function package_file_template(name, version) {
  return "\n  {\n    \"name\": \"" + name + "\",\n    \"version\": \"" + version + "\",\n    \"dependencies\": {\n      \"axios\": \"^0.11.0\",\n      \"routest\": \"^0.3\",\n      \"sdkarate\": \"0.0.3\"\n    }\n  }\n  ";
}