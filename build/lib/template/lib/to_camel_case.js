'use strict';

Object.defineProperty(exports, "__esModule", {
        value: true
});
exports.default = toCamelCase;
function toCamelCase(str) {
        // http://stackoverflow.com/a/2970588
        return str.replace(/\s(.)/g, function ($1) {
                return $1.toUpperCase();
        }).replace(/\s/g, '').replace(/^(.)/, function ($1) {
                return $1.toUpperCase();
        });
}