'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = sdk_template;

var _to_camel_case = require('./lib/to_camel_case');

var _to_camel_case2 = _interopRequireDefault(_to_camel_case);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function sdk_template(name) {
  var api_name = (0, _to_camel_case2.default)(name);
  var api_routes = api_name + 'API';
  var sdk_name = api_name + 'SDK';
  var filename = 'router.' + name + '.js';

  return '\nvar ' + api_name + 'Definition = require(\'./lib/routes.' + name + '\');\n\nvar ' + api_name + '    = new ' + api_name + 'Definition();\n\nvar SDKarate    = require(\'sdkarate\')\n\nvar axios       = require(\'axios\');\n\nfunction routify(route_path, args){\n  var arg;\n  for(arg in args){\n    route_path = route_path.replace(":"+arg, args[arg]);\n  }\n  return route_path;\n}\n\nfunction make_url(url){\n  return url.scheme+url.host+\':\'+url.port+url.path\n}\n                     \nvar ' + api_name + 'SDK = SDKarate(' + api_name + ', {}, (function(' + name + '){\n   return function(api, action, SDK){\n      SDK[action] = function(options){\n        return ' + name + ' \n              .__definition\n              .then(function(def){\n                var route = ' + name + '.__methods[action]\n                var url = make_url(def._options);\n                return axios.create({\n                      baseURL: url,\n                      headers: def._options.headers,\n                      validateStatus: function (status) {\n                                        return status >= 200 && status < 300; // Reject only if the status code is greater than or equal to 500\n                                      }\n                  }).request({\n                    method: route.method,\n                    url: routify(route.path, options.route),\n                    params: options.query,\n                    data: options.body,\n                    headers: options.headers||{}\n                  })\n              })\n              .then(function(result){ return result.data } )\n              .catch(function(response){\n                return Promise.reject({data: response.data,\n                                       request: options,\n                                       status: response.status,\n                                       statusText: response.statusText,\n                                       headers: response.headers,\n                                       config: response.config } )\n              })\n    }\n    return SDK;\n  }\n})(' + api_name + 'Definition));\n\nexport default ' + sdk_name + '\n';
}