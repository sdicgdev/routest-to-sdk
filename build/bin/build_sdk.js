#!/usr/bin/env node
'use strict';

var _fs = require('../lib/fs');

var _build_sdk = require('../lib/build_sdk');

var _build_sdk2 = _interopRequireDefault(_build_sdk);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var BUILD_DIR = process.cwd() + '/build/sdk';
var SDK_LIB = BUILD_DIR + '/lib';
var package_info = require(process.cwd() + '/package.json');
var sdk_info = package_info.sdk_info;
if (Array.isArray(sdk_info)) {
  var info_array = sdk_info;
} else {
  var _info_array = [sdk_info];
}

var opts_ray = sdk_info.map(function (info) {
  var package_name = info.name;
  var routes_loc = process.cwd() + '/' + info.definition;
  var SDK_FILE = BUILD_DIR + '/sdk.' + package_name + '.js';
  var ROUTER_FILE = BUILD_DIR + '/router.' + package_name + '.js';
  var RUNNER_FILE = BUILD_DIR + '/runner.' + package_name + '.js';
  return {
    package_name: package_name,
    routes_loc: routes_loc,
    BUILD_DIR: BUILD_DIR,
    SDK_LIB: SDK_LIB,
    SDK_FILE: SDK_FILE,
    ROUTER_FILE: ROUTER_FILE,
    RUNNER_FILE: RUNNER_FILE
  };
});

(0, _fs.cleanup)(BUILD_DIR).then(function (_) {
  return _build_sdk2.default.apply(undefined, _toConsumableArray(opts_ray));
}).then(function (_) {
  return (0, _build_sdk.define_package)(BUILD_DIR + '/package.json', package_info);
}).catch(function (e) {
  console.log("ERROR:", e);
  process.exit(1);
});